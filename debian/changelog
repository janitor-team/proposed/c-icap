c-icap (1:0.5.6-2) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Fix field name typos in debian/copyright.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 01 Feb 2021 22:23:48 +0000

c-icap (1:0.5.6-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 0.5.6
  * Update Standards-Version to 4.5.0
  * Change to debhelper-compat
  * Bump debhelper to 12
  * Move Pre-depends field from libicapapi5 to c-icap in d/control
  * Fix tab character in d/copyright
  * Update d/libicapapi5.symbols
  * autopkgtest toolbox.sh, add sleep before reading pid file

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 08 Mar 2020 08:23:51 +0100

c-icap (1:0.5.3-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Mathieu Parent ]
  * Orphaning package (See: #907670)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 11 Oct 2019 19:00:58 +0200

c-icap (1:0.5.3-2) unstable; urgency=medium

  * Upload to unstable

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Sep 2018 16:26:07 +0200

c-icap (1:0.5.3-1) experimental; urgency=medium

  * Upload to experimental
  * New upstream version 0.5.3
    - Bumped API to libicapapi5
    - Update symbols
  * Add openssl support
  * Add bz2 and brotli support
  * Lintian and other housekeeping:
    - Update Vcs-* fields to salsa
    - Standards-Version: 4.2.1
    - Remove libicapapi*-dbg (replaced by -dbgsym)
    - Run wrap-and-sort
    - Priority: extra -> optional
    - Move libicapapi5 to section libs
    - Fix insecure-copyright-format-uri
    - Remove trailing whitespaces in debian/changelog
    - Debhelper compat 9 -> 11

 -- Mathieu Parent <sathieu@debian.org>  Sun, 02 Sep 2018 21:11:31 +0200

c-icap (1:0.4.4-1) unstable; urgency=medium

  * New upstream release

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Oct 2016 21:27:22 +0200

c-icap (1:0.4.3-2) unstable; urgency=medium

  * Add autopkgtest tests

 -- Mathieu Parent <sathieu@debian.org>  Fri, 20 May 2016 07:20:36 +0200

c-icap (1:0.4.3-1) unstable; urgency=medium

  * New upstream release
  * 0001-fix-config-prefix.patch: Update
  * 0004-man-page-fixes.patch: Merged upstream
  * 0008-Fix-ModulesDir-and-ServicesDir-on-MultiArch.patch: Merged upstream
  * Standards-Version: 3.9.8, no change
  * Set hardening to +all (which actually enable pie)

 -- Mathieu Parent <sathieu@debian.org>  Mon, 16 May 2016 21:47:31 +0200

c-icap (1:0.4.2-3) unstable; urgency=medium

  * Suggest squid instead of squid3 (Closes: #817136)
  * Fix vcs-field-uses-insecure-uri
  * Standards-Version: 3.9.7

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Mar 2016 16:03:28 +0100

c-icap (1:0.4.2-2) unstable; urgency=medium

  * Patches:
    - Use @libdir@/c_icap instead of overriden @libexecdir@
  * Take ownership of the package. Thanks Tim and Jochen
  * Update patches
  * properly detect shm on kfreebsd / hurd (Closes: #808084)

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Jan 2016 07:00:43 +0100

c-icap (1:0.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Mathieu Parent <sathieu@debian.org>  Wed, 07 Oct 2015 07:59:46 +0200

c-icap (1:0.4.1-1) unstable; urgency=medium

  * Imported Upstream version 0.4.1
  * Build with PCRE
  * Patches
    - 0004-man-page-fixes.patch updated
    - 0010-Rename-CONF-to-C_ICAP_CONF.patch dropped, similar merged upstream
  * Bumped API to libicapapi4
  * gbp.conf: renamed git-buildpackage section to buildpackage

 -- Mathieu Parent <sathieu@debian.org>  Sun, 20 Sep 2015 01:52:30 +0200

c-icap (1:0.3.5-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 3.9.6, no change

 -- Mathieu Parent <sathieu@debian.org>  Tue, 16 Jun 2015 09:48:14 +0200

c-icap (1:0.3.4-2) unstable; urgency=medium

  * Rename CONF to C_ICAP_CONF to fix FTBFS of c-icap-modules due to redeclared
    CONF (Closes: #768684)

 -- Mathieu Parent <sathieu@debian.org>  Wed, 12 Nov 2014 18:33:18 +0100

c-icap (1:0.3.4-1) unstable; urgency=medium

  * New upstream release
  * Fix lintian pre-depends-directly-on-multiarch-support
  * Add Vcs-* fields
  * Imported Upstream version 0.3.4

 -- Mathieu Parent <sathieu@debian.org>  Fri, 22 Aug 2014 21:29:45 +0200

c-icap (1:0.3.3-3) unstable; urgency=medium

  * Remove bashism in init script (Closes: #751484)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 20 Jun 2014 14:48:05 +0200

c-icap (1:0.3.3-2) unstable; urgency=medium

  * Harden config parsing in init script (Closes: #743202)

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Apr 2014 09:53:03 +0200

c-icap (1:0.3.3-1) unstable; urgency=medium

  * New upstream release

 -- Mathieu Parent <sathieu@debian.org>  Wed, 12 Mar 2014 08:07:14 +0100

c-icap (1:0.3.2-1) unstable; urgency=medium

  * New upstream release
  * Drop merged patches:
    - 0003-fix-spelling-error-in-binary.patch
    - 0005-additional-manpage-fix.patch
    - 0006-Restrict-permissions-on-fifo.patch
    - 0007-Correctly-daemonize.patch
  * Update partially merged patches
    - 0004-man-page-fixes.patch
  * Use dh-autoreconf (Closes: #727334)

 -- Mathieu Parent <sathieu@debian.org>  Sat, 25 Jan 2014 18:54:08 +0100

c-icap (1:0.3.1-2) unstable; urgency=medium

  * Mention upstream bugs in patches
  * Mention public-domain md5.c (Closes: #732557)
  * Fix license, from LGPL-2.1 to LGPL-2.1+

 -- Mathieu Parent <sathieu@debian.org>  Sat, 21 Dec 2013 10:42:45 +0100

c-icap (1:0.3.1-1) unstable; urgency=low

  * Take package ownership, as Tim is very busy (see #709151)
  * New upstream (Closes: #709151)
    - Build with ld --as-needed (Closes: #639835)
    - Updated config.{sub,guess} (Closes: #727334)
    - Upstream claims they have fixed Mayhem bugs (Closes: #715706, #715707)
  * Remove 0002-ld-as-needed.patch
  * Updated 0004-man-page-fixes.patch
  * Updated 0003-fix-spelling-error-in-binary.patch
  * API version 0 -> 3
  * Add me to uploaders
  * debian/control: Bump Standards version to 3.9.5
  * debian/copyright: Update Copyright format URL
  * Move to debhelper 9:
    - debian/compat: changed from 7 to 9
    - debian/control: Build-Depends raised
  * Multi-Arch (from dh 9):
    - debian/*.install: Use multiarch path
    - debian/control: Pre-Depend multiarch-support for libicapapi3
    - debian/rules: Plugin path is multi-arch also
    - Add a patch to fix ModulesDir and ServicesDir
  * Hardening(from dh 9)
    - debian/rules: Warning on all and bindnow hardening
  * debian/control: Removing obsolete DM-Upload-Allowed  lintian warning
  * init, complete rewrite
  * Set /var/run/c-icap owner and group from config (Closes: #676363)
  * Adapt debian/c-icap.postrm to match
    http://wiki.debian.org/AccountHandlingInMaintainerScripts
  * Create the c-icap group and run the server with it
  * Restrict permissions on /var/run/c-icap/c-icap.ctl (Closes: #645122)
  * Correctly daemonize (Closes: #645310)
  * Suggests libc-icap-module instead of namely all modules
  * Add gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Wed, 18 Dec 2013 13:00:58 +0100

c-icap (1:0.1.6-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: cp: cannot stat `debian/tmp/etc/c-icap/c-icap.conf': No
    such file or directory": use override target in debian/rules.
    (Closes: #666329)

 -- gregor herrmann <gregoa@debian.org>  Tue, 17 Apr 2012 22:00:09 +0200

c-icap (1:0.1.6-1) unstable; urgency=low

  * New upstream version
  * debian/control: Bump Standards version to 3.9.2
  * debian/control: Added DM-Upload-Allowed
  * debian/rules: Remove unneeded .la files
  * debian/c-icap.init.d:
    + Added STARTUPTIME and use DODTIME in init script
    + DODTIME in Daemon stop should close LP: #808315

 -- Tim Weippert <weiti@weiti.org>  Sun, 14 Aug 2011 17:09:21 +0200

c-icap (1:0.1.5-1) unstable; urgency=low

  * New upstream version
  * debian/c-icap.init.d: Increase DODTIME from 1 to 3 ( restart problems)

 -- Tim Weippert <weiti@weiti.org>  Thu, 31 Mar 2011 19:33:58 +0200

c-icap (1:0.1.4-2) unstable; urgency=low

  * debian/control: Added jochen@debian.org to Uploaders
  * debian/control: rewritten to match DEP5 Syntax
  * debian/control: Added build dep libdb-dev to build mkbdb binary
  * debian/c-icap.install: Added binary c-icap-mkbdb
  * Add Patch to fix lintian complain: spelling-error-in-binary
    + debian/patches/0005-fix-spelling-error-in-binary.patch
    + debian/patches/0006-man-page-fixes.patch
    + debian/patches/0007-additional-manpage-fix.patch
  * Remove libicapapi.la

 -- Tim Weippert <weiti@weiti.org>  Sun, 13 Feb 2011 18:38:44 +0100

c-icap (1:0.1.4-1) unstable; urgency=low

  * New upstream release (Closes: #607836)
  * Fix on purge c-icap, now really (Closes: #589001)
  * Fix 2 lintian errors
  * Fix FTBFS with ld --as-needed merge from Ubuntu (Closes: #608156)
  * Added symbols file for libicapapi0
  * Cleanup control file

 -- Tim Weippert <weiti@weiti.org>  Tue, 28 Dec 2010 16:58:05 +0100

c-icap (1:0.1.3-3) unstable; urgency=low

  * Fix permission on /var/run/c-icap
  * Fix Error on purge c-icap

 -- Tim Weippert <weiti@weiti.org>  Tue, 14 Dec 2010 10:27:09 +0100

c-icap (1:0.1.3-2) unstable; urgency=low

  * Added Patch from Jochen Friedrich, fix problems on sparc
  * Fix include installation directory to /usr/include/c_icap

 -- Tim Weippert <weiti@weiti.org>  Mon, 13 Dec 2010 20:05:59 +0100

c-icap (1:0.1.3-1) unstable; urgency=low

  * New upstream version
  * New Maintainer
  * Clean up patches, -dbg package
  * Fix "Links against libclamav", remove modules from core package
    (Closes: #582311)

 -- Tim Weippert <weiti@weiti.org>  Thu, 18 Nov 2010 18:45:42 +0100

c-icap (20080706rc3-2) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * Bump Standards version to 3.8.4
  * Fix init script dependency on $remote_fs

 -- Jochen Friedrich <jochen@scram.de>  Fri, 07 May 2010 11:22:32 +0200

c-icap (20080706rc3-1) unstable; urgency=low

  * New upstream version. (Closes: #556100)
  * Completed debianization.
  * First upload to Debian.

 -- Jochen Friedrich <jochen@scram.de>  Mon, 16 Nov 2009 16:04:15 +0100

c-icap (20070418-1) unstable; urgency=low

  * Initial release

 -- Yauhen Kharuzhy <jekhor@gmail.com>  Wed, 31 Oct 2007 17:59:04 +0200
